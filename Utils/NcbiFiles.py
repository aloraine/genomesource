"""Functions for working with data files from Entrez and the NCBI Web site"""

import re,sys,os

# in common
import Utils.General as gen_utils

species2code = {"mouse":"10090"}
sep1 = '\t'
sep2 = ','

def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass


def get_acc2gene(fn='../annots/gene2accession.gz',species=None):
    """
    Function:
    Returns : dictionary mapping accessions onto Entrez Gene ids
    Args    : fn - name of the Entrez Gene file, gene2accession
    """
    sp_code = species2code[species]
    fh = gen_utils.readfile(fn)
    acc2gene = {}
    while 1:
        line = fh.readline()
        if not line:
            break
        toks = line.rstrip().split(sep1)
        if not toks[0]==sp_code:
            continue
        acc = toks[3].split('.')[0]
        if acc == '-':
            continue
        entrez_id = toks[1]
        if not acc2gene.has_key(acc):
            acc2gene[acc]=[entrez_id]
        elif not entrez_id in acc2gene[acc]:
            acc2gene[acc].append(entrez_id)
    return acc2gene

def get_g2sym(fn='../data/gene_info.gz'):
    inf = gen_utils.readfile(fn)
    inf.readline()
    genes = {}
    id_field = 1
    sym_field = 2
    type_field = 9
    while 1:
        line = inf.readline()
        if not line or line == "":
            break
        line = line.rstrip()
        toks = line.split('\t')
        gene_id = toks[id_field]
        symbol=toks[sym_field]
        if not genes.has_key(gene_id):
            genes[gene_id]=symbol
    inf.close()
    return genes

def get_gene2info(f='../annots/gene_info.gz',species_code=None):
    """
    Function: 
    Returns : a dictionary with Entrez Gene ids as keys to
              a sub-dictionary with keys 'symbol', 'descr',
              'line' (the original line where the data were obtained)
    Args    : f - name of the Entrez gene file with daa (gene_info.gz)
              species_code - the species
    """
    inf = gen_utils.readfile(f)
    genes = {}
    descr_field = 8
    id_field = 1
    sym_field = 2
    species_field = 0
    type_field = 9
    while 1:
        line = inf.readline()
        if not line or line == "":
            break
        line = line.rstrip()
        toks = line.split('\t')
        if toks[species_field]!=species_code or \
           toks[type_field]!='protein-coding':
            continue
        gene_id = toks[id_field]
        descr = toks[descr_field]
        symbol=toks[sym_field]
        if not genes.has_key(gene_id):
            genes[gene_id]={'symbol':symbol,
                            'descr':descr,
                            'line':line}
    return genes

def get_gene2refseq(f='../annots/gene2refseq.gz',
                    species_code=None):
    """
    Function: get a mapping between Entrez Gene ids and
              associated RefSeq (NM) accessions
    Returns : dictionary where keys are Entrez Gene ids
              and values are lists of NM_ ids
    Args    : f - gene2refseq, downloaded from NCBI ftp size
              species_code - taxonomy id for the species we want

    Retrieves protein-coding only.
    If species_code not given, we get all the ids.

    some tax ids:
    
    mouse - 10090
    human - 9606

    """
    inf = gen_utils.readfile(f)
    genes = {}
    id_field = 1
    species_field = 0
    nm_field = 3
    np_field = 5
    while 1:
        line = inf.readline()
        if not line or line == '':
            break
        line = line.rstrip()
        toks = line.split(sep1)
        # NP_ filter eliminates non-protein-coding genes
        if not toks[species_field] == tax_id or \
           not toks[np_field].startswith('NP_'):
            continue
        gene_id = toks[id_field]
        nm = toks[nm_field]
        if not genes.has_key(gene_id):
            genes[gene_id]=[]
        if not nm in genes[gene_id]:
            genes[gene_id].append(nm)
    return genes

def count_vals(dct,max=5):
    """
    Function: Tally number of keys with 1,2,3 etc items in the
              keyed list.
    Returns : a dictionary of counts
    Args    : dct - a dictionary of lists, e.g., output from
                    get_gene2refseq
              max - count all lists with max or more items the same

    Use this to get a distribution of genes with different numbers of
    RefSeq mRNAs, a very rough picture of alternative transcription
    """
    counts = {}
    for key in dct.keys():
        lst = dct[key]
        i = len(lst)
        if i >= max:
            i = max
        if not counts.has_key(i):
            counts[i]=0
        counts[i]=counts[i]+1
    return counts

def get_refseq2entrez(f='../data/gene2refseq.gz',taxid=None):
    """
    Function: Get a mapping between RefSeq ids and Entrez Gene ids
    Returns : dictionary mapping RefSeq ids onto Entrez Gene ids
    Args    : gene2refseq.gz - from NCBI Gene ftp data site
              taxid - one or more taxonomy ids we want

    mouse - 10090
    fruit fly -
    human - 9606
    """
    inf = readfile(f)
    d = {}
    while 1:
        line = inf.readline()
        if not line:
            break
        toks = line.rstrip().split('\t')
        if not taxid or toks[0] in taxid:
            gid = toks[1]
            rid = toks[3]
            if not rid == '-':
                acc = rid.split('.')[0]
                if d.has_key(acc) and not d[acc] == gid:
                   sys.stderr.write("Warning: multiple entries for RefSeq: " \
                                   + acc + os.linesep)
                else:
                    d[acc]=gid
    inf.close()
    return d
                    
