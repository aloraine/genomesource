"Utilities for working with a MySQL database. Requires MySQLdb"

import os


def makeDbConnection(password=None,
                     database=None,
                     host=None,
                     user=None):
    "Make a database connection. Use environment variables HOST, DBPASS, DB, DBUSER if necessary. Returns a MySQLdb.cursors.DictCursor."
    env = os.environ
    if not password:
        password = os.environ["DBPASS"]
    if not database:
        database = os.environ["DB"]
    if not host:
        host = os.environ["DBHOST"]
    if not user:
        user = os.environ["DBUSER"]
    if not user or not host or not database or not password:
        raise IllegalArgumentException("Provide user, database, host, password to make a database connection.")
    import MySQLdb
    import MySQLdb.cursors
    db = MySQLdb.connect(host=host,user=user,passwd=password,db=database,cursorclass=MySQLdb.cursors.DictCursor)
    return db
