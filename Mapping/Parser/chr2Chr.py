"""
Many years ago, I renamed Arabidopsis chromosomes Chr1,2,etc to chr1,2,etc. Now I'm changing the names back to
the upper-case versions to make the names more consistent with what everyone else uses.
"""

import os

def lsFiles():
    files=os.listdir()
