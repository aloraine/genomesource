"""
Functions for reading GFF3 files from the Ensembl system.

Example feature types:

CDS
RNase_MRP_RNA
SRP_RNA
chromosome
exon
five_prime_UTR
gene
lnc_RNA
mRNA
ncRNA_gene
pre_miRNA
rRNA
snRNA
snoRNA
tRNA

"""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys
import warnings

def gff2feats(fname=None,fh=None):
    """
    Function: read features from Ensembl GFF3 format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              RNA transcripts
    Args    : fname - name of file to read or
              fh - an opened filehandle 
    """
    lineNumber = 0
    if not fh and fname:
        fh = utils.readfile(fname)
    else:
        raise ValueError("gff2feats requires name of file or file stream.")
    RNAs = {}
    sub_feats={}
    genes={}
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        try:
            (seqname,producer,feat_type,start,end,
             ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        key_vals=None
        if feat_type.endswith("gene"):
            key_vals = parseKeyVals(extra_feat)
            gene_id=key_vals['ID']
            genes[gene_id]=key_vals
            continue
        start = int(start)
        end = int(end)
        start = start-1
        length=end-start
        if strand == '+':
           strand = 1
        elif strand == '-':
           strand = -1
        elif strand == '.':
          strand = None # strand is not relevant or we don't know what it is
        if feat_type.endswith('RNA'):
            key_vals = parseKeyVals(extra_feat)
            display_id=key_vals['ID']
            feat = feature.CompoundDNASeqFeature(seqname=seqname,display_id=display_id,start=start,length=length,strand=strand,key_vals=key_vals,feat_type=feat_type)
            RNAs[display_id]=feat
            continue
        if feat_type in ['exon','CDS']:
            key_vals = parseKeyVals(extra_feat)
            feat = feature.DNASeqFeature(seqname=seqname,start=start,length=length,strand=strand,key_vals=key_vals,feat_type=feat_type)
            parent_ID=key_vals['Parent']
            if not sub_feats.has_key(parent_ID):
                sub_feats[parent_ID]=[]
            sub_feats[parent_ID].append(feat)
    for parent_ID in sub_feats.keys():
        RNA = RNAs[parent_ID]
        for sub_feat in sub_feats[parent_ID]:
            RNA.addFeat(sub_feat)
    for RNA_id in RNAs.keys():
        RNA = RNAs[RNA_id]
        gene_id=RNA.getVal("Parent")
        if genes[gene_id].has_key("description"):
            RNA.setKeyVal("description",genes[gene_id]["description"])
    return RNAs.values()


def parseKeyVals(extra_feat):
    labels=[]
    values=[]
    in_label=False
    in_value=False
    in_quoted_value=False
    N = len(extra_feat)
    prev=None
    current=None
    i = 0
    while i < N:
        current=extra_feat[i]
        #sys.stderr.write("current: %s\n"%current)
        if in_label:
            #sys.stderr.write("in_label\n")
            if current=='=': # signals start of value
                #sys.stderr.write("Resetting in_label to false, in_value to True, in_quoted_value to False\n")
                in_label=False
                in_value=True
                values.append("")
                in_quoted_value=False
            else:
                labels[-1]=labels[-1]+current # add character to current label
                #sys.stderr.write("added %s to current label, making: %s\n"%(current,labels[-1]))
        elif in_value:
            #sys.stderr.write("in_value\n")
            if current == '"': # signals start of value, end of value, or it's just another character in value
                if prev == '=': # ....=" start of quoted value
                    in_quoted_value=True
                elif in_quoted_value: # end of quoted value
                    in_quoted_value=False
                else:
                    values[-1]=values[-1]+current # just another character in value; add to current value
                    #sys.stderr.write("added %s to current value, making: %s\n"%(current,values[-1]))
            elif current == ";":
                if in_quoted_value:
                    values[-1]=values[-1]+current # just another character in value; add to current value
                    #sys.stderr.write("added %s to current value, making: %s\n"%(current,values[-1]))
                else:
                    #sys.stderr.write("current is %s, reached end of value, set in_value to False\n"%current)
                    in_value=False # reached end of value
            else:
                values[-1]=values[-1]+current
                #sys.stderr.write("added %s to current value, making: %s\n"%(current,values[-1]))
        else:
            #sys.stderr.write("not in value or label; start new label\n")
            if not current== ";": # pathological case of foo=bar;;;baz=whatever; Apple genome GFF3
                labels.append(current) # not in value or label, start a new label
                in_label=True
        i=i+1
        prev=current
    to_return = {}
    i = 0
    while i < len(labels):
        to_return[labels[i]]=values[i]
        i=i+1
    return to_return
        
