#!/usr/bin/env python

"Read Ensembl gene model GFF3 file and write out as BED14 (BED detail) format."

import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.EnsemblGff3 as p

def readGffFile(fname=None):
    models = p.gff2feats(fname)
    return models

def prepareModels(feats):
    models = []
    for feat in feats:
        model = prepareModel(feat)
        models.append(model)
    return models

def prepareModel(feat):
    key_vals=feat.getKeyVals()
    gene_name = key_vals["Parent"].split(":")[1]
    key_vals["gene_name"]=gene_name
    feat.setDisplayId(key_vals["transcript_id"])
    biotype = key_vals['biotype']
    description = None
    if key_vals.has_key("description"):
        description = key_vals["description"]
    if description:
        description = description + " biotype: " + biotype
    else:
        description = "biotype: " + biotype
    feat.setKeyVal("Note",description)
    return feat

def writeBedFile(fname=None,feats=None):
    Bed.feats2bed(feats,fname=fname,bed_format='BED14')

def convert(bed_file=None,
            gff_file=None):
    feats = readGffFile(fname=gff_file)
    feats = prepareModels(feats)
    writeBedFile(fname=bed_file,feats=feats)

def main(bed_file=None,gff_file=None):
    convert(bed_file=bed_file,
            gff_file=gff_file)

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gff_file",help="GFF3 file to convert. Can be compressed. [required]",dest="gff_file"),
    parser.add_option("-b","--bed_file",help="BED14 (bed detail) format file to write [required]",
                      dest="bed_file",default=None)
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gff_file:
        parser.error("Bed or GFF files not specified.")
    main(gff_file=options.gff_file,
         bed_file=options.bed_file)
