#!/usr/bin/env python

"""
Use this to remove non-ASCII characters from a file. 

Accepts input on stdin and writes results to stdout.

See:

http://stackoverflow.com/questions/2743070/removing-non-ascii-characters-from-a-string-using-python-django

Check that it worked using cut, which prints an error message when given a line of text w/ a non-ASCII character.

Invoke as:

$ cat file.txt | cleanNonAsciiChars.py > newfile.txt
"""
import sys,fileinput

def main():
    N = 0 
    for line in fileinput.input():
        newline = strip_non_ascii(line)
        if newline != line:
            sys.stderr.write(line)
            N=N+1
        sys.stdout.write(newline)
    sys.stderr.write("cleaned %i lines\n"%N)

                        
def strip_non_ascii(string):
    ''' Returns the string without non ASCII characters'''
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)

if __name__ == '__main__':
    main()
