#!/bin/bash

# genome versions we support that 
# came from UCSC, excluding microbes
DIRS="A_gambiae_Feb_2003 
A_melanoleuca_Dec_2009 
A_mellifera_Jan_2005 
B_taurus_Oct_2011 
C_elegans_May_2008 
C_elegans_Oct_2010
C_elegans_Feb_2013
C_porcellus_Feb_2008 
C_sabeus_Mar_2014
D_melanogaster_Apr_2006 
D_rerio_May_2017
D_rerio_Jul_2010
E_caballus_Sep_2007
E_coli_K12-MG1655_Jan_2012 
F_catus_Dec_2008
F_catus_Sep_2011
G_gallus_Nov_2011 
H_sapiens_Dec_2013
H_sapiens_Feb_2009
H_sapiens_Mar_2006
H_sapiens_May_2004
L_africana_Jul_2009 
M_musculus_Dec_2011
M_musculus_Jul_2007
M_musculus_Feb_2006 
M_musculus_Aug_2005 
N_vectensis_Jun_2007
O_aries_Feb_2010 
P_troglodytes_Oct_2010
R_norvegicus_Mar_2012 
S_cerevisiae_Apr_2011
S_cerevisiae_Jun_2008
T_rubripes_Oct_2011 
X_tropicalis_Nov_2009"

for DIR in $DIRS
do
    if [ -d $DIR ]
    then
        writeQuickLoadHeaderUCSC.py $DIR > $DIR/HEADER.md
    fi
done
